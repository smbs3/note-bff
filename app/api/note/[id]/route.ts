import { createClient } from '@/utils/supabase/server';
import {NextResponse , NextRequest} from 'next/server'

export async function POST(req:NextRequest, context:any) {
    const supabase = createClient();
    const {params}= context;

    const { data, error } = await supabase.from('notes').update({deleted: true}).eq("id", params.id).select()
    
    if (error) {
        return NextResponse.json({ error: error.message }, { status: 500 });
    }
    
    return NextResponse.json({
        status:200,
        message: 'Nota eliminada exitosamente'
    });
}

export async function PATCH(req:NextRequest, context:any){

    const supabase = createClient();
    const {params} = context      
    
    const {title, content} = await req.json();

    console.log(title , params.id);
    
    const { data, error } = await supabase.from('notes').update({title, content}).eq("id", params.id).select()
    
    if (error) {
        return NextResponse.json({ error: error.message }, { status: 500 });
    }
    
    return NextResponse.json({
        status:200,
        message: 'Nota Actualizada exitosamente'
    });
}

export async function GET(req:NextRequest, context:any){

    const supabase= createClient();
    
    const {params} = context;

     const {data, error} = await supabase.from('notes').select().eq('id', params.id);

     if (error) {
      return NextResponse.json({ error: error.message }, { status: 500 });
  }
  
     return NextResponse.json({
        data
     })
}
