import { createClient } from '@/utils/supabase/server';
import { redirect } from 'next/dist/server/api-utils';
import {NextResponse , NextRequest} from 'next/server'

export async function GET(){
    const supabase= createClient();
    const {data: { user }} = await supabase.auth.getUser();
    if (!user) {
        return;
      }
     const {data: notes, error} = await supabase.from('notes').select('*').eq('user_id', user.id);

     if (error) {
      return NextResponse.json({ error: error.message }, { status: 500 });
  }
  
     return NextResponse.json({
        notes
     })
}

export async function POST(req:NextRequest, context:any){
   const supabase = createClient();
     
   const { title, content } = await req.json();
   const {data: { user }} = await supabase.auth.getUser();
 
   if (!user) {
    return;
  }
   const { data, error } = await supabase.from('notes').insert([{ title, content, user_id:user.id}]);
 
   if (error) {
       return NextResponse.json({ error: error.message }, { status: 500 });
   }
   
   return NextResponse.json({
       message: 'Nota creada exitosamente',
       data
   });
 }