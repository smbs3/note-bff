'use client'
import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/navigation';

interface Note {
    id: string | any;
    title: string;
    content: string;
    deleted:boolean;
  }

const page = ({ params }: {
    params: { id: string }
}) => {
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');

    const router = useRouter();

    useEffect(() => {
        fetchNote();
      }, []);


      const fetchNote = async () => {
        const id = params.id;

        const response = await fetch(`/api/note/${id}`);
    
        const data = await response.json();
        console.log()
        setTitle(data.data[0].title);
        setContent(data.data[0].content);

        
      };
    const updateNote = async () => {
        const id = params.id;



        await fetch(`/api/note/${id}`, {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ title, content }),
        });
        setTitle('');
        setContent('');
        router.push('/notes')

    };
    const cancelEdit = () => {
        setTitle('');
        setContent('');
        router.push('/notes')
    };

    return (
        <div>
            <input
                className="border focus:outline-none focus:border-blue-500 border-gray-300 text-black rounded-md px-3 py-2 mb-2 w-full"
                type="text"
                placeholder="Título"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
            />
            <textarea
                className="border focus:outline-none focus:border-blue-500 border-gray-300 text-black rounded-md px-3 py-2 mb-2 w-full"
                placeholder="Contenido"
                value={content}
                onChange={(e) => setContent(e.target.value)}
            />
            <button className="bg-blue-500 hover:bg-blue-600 text-white rounded-md px-4 py-2 mr-2"
                onClick={updateNote}
            >
                Guardar Cambios
            </button>
            <button
                className="bg-gray-500 hover:bg-gray-600 text-white rounded-md px-4 py-2"
                onClick={cancelEdit}
            >
                Cancelar
            </button>
        </div>
    )
}

export default page