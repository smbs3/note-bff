'use client'

import React, { useState } from 'react'
import { useRouter } from 'next/navigation'


const Note = () => {
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const router = useRouter();

    const addNote = async () => {
        await fetch('/api/notes', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ title, content}),
        });
        setTitle('');
        setContent('');
       router.push('/notes')
      };

  return (
    <div>
        <input 
        className="border focus:outline-none focus:border-blue-500 border-gray-300 text-black rounded-md px-3 py-2 mb-2 w-full"
        type="text" 
        placeholder="Título" 
        value={title} 
        onChange={(e) => setTitle(e.target.value)} 
      />
      <textarea 
        className="border focus:outline-none focus:border-blue-500 border-gray-300 text-black rounded-md px-3 py-2 mb-2 w-full"
        placeholder="Contenido" 
        value={content} 
        onChange={(e) => setContent(e.target.value)} 
      />
      <button onClick={addNote} className="bg-green-500 hover:bg-green-600 text-white rounded-md px-4 py-2"> Agregar Nota </button>
    </div>
  )
}

export default Note