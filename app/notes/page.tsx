'use client'
import { useRouter } from 'next/navigation';
import { useState, useEffect } from 'react';

interface Note {
  id: string | any;
  title: string;
  content: string;
  deleted: boolean;
}

const Notes = (): any => {
  const [notes, setNotes] = useState<Note[]>([]);
  const router = useRouter();


  useEffect(() => {
    fetchNotes();
  }, []);

  const fetchNotes = async () => {
    const response = await fetch('/api/notes');

    const data = await response.json();
    setNotes(data.notes);

  };



  const deleteNote = async (id: string) => {
    await fetch(`/api/note/${id}`, { method: 'POST' });
    fetchNotes();
  };




  const handleClick = (e: any) => {
    e.preventDefault()
    router.push('/note')
  }


  return (

    <div className="container pt-8">
      <h1 className="text-3xl mb-4">Notas</h1>
      <div className="relative overflow-x-auto ">
        <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3">
                title
              </th>
              <th scope="col" className="px-6 py-3">
                content
              </th>
              <th scope="col" className="px-6 py-3">
                Action
              </th>

            </tr>
          </thead>
          <tbody>
            {
              (notes || []).filter(note => !note.deleted).map((note) => {
                return (
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                      {note.title}
                    </th>
                    <td className="px-6 py-4">
                      {note.content}

                    </td>
                    <td className="px-6 py-4">
                      <button onClick={() => deleteNote(note.id)} className="bg-red-500 hover:bg-red-600 text-white rounded-md px-3 py-1 mr-2">Eliminar</button>
                      <button onClick={() => router.push(`/note/${note.id}`)} className="bg-blue-500 hover:bg-blue-600 text-white rounded-md px-3 py-1">Editar</button>
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
        <div className='mt-8'>
          <button className="bg-blue-500 hover:bg-blue-600 text-white rounded-md px-4 py-2 mr-2" onClick={handleClick}>Add New </button>
        </div>
      </div>
    </div>

  );
}

export default Notes